# CloudFish

CloudFish is a module for an aws lambda function to invoke scripts on ec2 instances via ssh.

# Aws

CloudFish uses ec2 instance tags to identify a pool of servers and uses the first availalbe within that pool to run a command.

The following should be set up within your aws account.

- An S3 bucket should be created to hold the ssh key to enable access to the ec2 instance.
- The pool of ec2 instances should be tagged with a given name/value.
- The lambda function user role should have read permissions to the ec2 instances to obtain a list of instances filtered by tag.
- The lambda function user role should have read permissions to the S3 bucket holding the key.

## Sample lambda function.

To make use of cloudfish the following sample function can be used.

```javascript
const cloudfish = require('cloudfish');

exports.handler = (event, context, callback) => {
    cloudfish({
        tagName: 'my-tag-name',
        tagValue: 'my-tag-value',
        s3Bucket: 'my-ssh-keys',
        s3Key: 'project/dev.pem',
        command: 'echo "And I, for one welcome our new insect overlords."'
    })
    .then(function(response) {
        callback(null, response);
    })
    .catch(function(err) {
        console.log(err.stack);
        callback(err.stack);
    });
};
```

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
