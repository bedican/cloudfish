const AWS = require('aws-sdk');
const fs = require('fs');
const Client = require('ssh2').Client;

const ec2 = new AWS.EC2();
const s3 = new AWS.S3();

function getInstances(chain) {

    return new Promise(function(resolve, reject) {

        var params = {
            Filters: [
                {
                    Name: 'instance-state-name',
                    Values: ['running']
                },
                {
                    Name: 'tag:' + chain.tagName,
                    Values: [ chain.tagValue ]
                }
            ]
        };

        ec2.describeInstances(params, function(err, data) {
            if (err) { reject(err); return; }

            var reservation, instance, tags;
            var instances = [];

            for (var x in data.Reservations) {
                reservation = data.Reservations[x];
                for (var y in reservation.Instances) {
                    instance = reservation.Instances[y];

                    instances.push({
                        id: instance.InstanceId,
                        ip: instance.PublicIpAddress
                    });
                }
            }

            chain.instances = instances;

            if (chain.instances.length) {
                resolve(chain);
            } else {
                reject(new Error('No instances found'));
            }
        });
    });
}

function getSshKey(chain) {
    return new Promise(function(resolve, reject) {

        var stream = fs.createWriteStream('/tmp/key.pem');

        var params = {
            Bucket: chain.s3Bucket,
            Key: chain.s3Key
        };

        s3.getObject(params)
            .on('success', function(response) {
                resolve(chain);
            })
            .on('error', function(response) {
                reject(new Error('Failed to getObject'));
            })
            .createReadStream()
            .pipe(stream);

    });
}

function runCommand(chain) {
    return new Promise(function(resolve, reject) {

        chain.stdout = [];
        chain.stderr = [];

        var conn = new Client();

        conn.on('ready', function() {
            conn.exec(chain.command, function(err, stream) {
                if (err) reject(err);

                stream.on('close', function(code, signal) {
                    conn.end();

                    if (code) {
                        reject(new Error('SSH command failed with code ' + code));
                    } else {
                        resolve(chain);
                    }

                }).on('data', function(data) {
                    chain.stdout.push(data.toString());
                }).stderr.on('data', function(data) {
                    chain.stderr.push(data.toString());
                });
            });
        })
        .connect({
            host: chain.instances[0].ip,
            port: 22,
            username: 'ubuntu',
            privateKey: fs.readFileSync('/tmp/key.pem')
        });
    });
}

module.exports = function(options) {

    var chain = {
        tagName: options.tagName || 'cloudfish',
        tagValue: options.tagValue || '1',
        s3Bucket: options.s3Bucket,
        s3Key: options.s3Key,
        command: options.command || 'uptime'
    };

    return getInstances(chain)
        .then(function(response) {
            return getSshKey(response);
        })
        .then(function(response) {
            return runCommand(response);
        });
};
